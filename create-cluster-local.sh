#!/bin/bash

# Install required packages
yay -S kind-bin
sudo pacman -S kubectl

# Create cluster
kind create cluster

# Get cluster info
export KUBECONFIG="$(kind get kubeconfig-path --name="kind")"
kubectl cluster-info
kubectl get all --all-namespaces

# Push image
# kind load docker-image shtr-demo:latest 

# Or configure private registry
# docker login your.private.registry
# kubectl create secret generic regcred --from-file=.dockerconfigjson=~/.docker/config.json --type=kubernetes.io/dockerconfigjson

# Deploy app
# kubectl apply -f deploy/app.yml

# Forward port
# docker run -d -p 5000:5000 --link kind-control-plane:k8s alpine/socat tcp-listen:5000,fork,reuseaddr tcp-connect:k8s:<svc-port>

# View your app at http://localhost:5000
